Il s'agit de la base de données de la page [Observatoire des salaires des enseignants dans l'Éducation nationale](https://blog.epicycle.fr/?page_id=678).
Celle-ci est sous license [Creative Commons Attribution](https://creativecommons.org/licenses/by/4.0/), le [code source](https://gitlab.com/observatoire_des_salaires/src) est quant à lui sous license [GNU GPL](https://www.gnu.org/licenses/gpl-3.0.en.html).
Merci de citer [blog.epicycle.fr](https://blog.epicycle.fr) en cas de réutilisation ou adaptation.
